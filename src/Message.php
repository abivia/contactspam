<?php

namespace Abivia\ContactSpam;

class Message
{
    public string $alert;
    public int $attempts;
    public string $country;
    public string $email;
    /**
     * @var string this is ipv4/XX where XX is a country code (or XX if country not found)
     */
    public string $fromId;
    public string $message;
    public string $name;
    public float $score;
    public string $status;
    public string $subject;

}
