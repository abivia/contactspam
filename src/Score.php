<?php
namespace Abivia\ContactSpam;


class Score
{
    protected string $configPath;
    protected Form $form;
    protected Message $message;

    public function __construct(Form $form, string $configPath) {
        $this->form = $form;
        $this->configPath = $configPath;
    }

    protected function isSpam(): bool
    {
        $spamTable = file_exists($this->configPath) ? include $this->configPath : [];
        $killItAt = $spamTable['deleteThreshold'] ?? 5.0;
        $this->message->score = 0;
        $target = '';
        foreach (['country', 'email', 'message', 'subject'] as $element) {
            $haystack = $this->message->$element;
            foreach ($spamTable[$element] ?? [] as $target => $weight) {
                $count = 0;
                if ($target[0] === '!') {
                    $count = preg_match_all($target, $haystack);
                } elseif (stripos($haystack, $target) !== false) {
                    $count = 1;
                }
                if ($count) {
                    if (is_numeric($weight)) {
                        $this->message->score += $count * $weight;
                    } elseif (isset($spamTable['actions'][$weight])) {
                        $fn = $spamTable['actions'][$weight];
                        $this->message->score += $fn($element, $haystack);
                    }
                }

                // Short circuit as soon as we cross the threshold.
                if ($this->message->score >= $killItAt) {
                    break 2;
                }
            }
        }
        if (($spam = $this->message->score >= $killItAt)) {
            ($this->form->logger)(
                "spam ({$this->message->score}): hit[$element]=$target"
            );
        }
        return $spam;
    }

    public function score(array $post): Message
    {
        $fromIpAddress = $_SERVER['REMOTE_ADDR'];
        $this->message = new Message();
        $this->message->status = 'good';
        $this->message->alert = '';
        $this->message->score = 0;
        $this->message->attempts = $this->form->get('attempts', 0);

        // Extract email and subject for logging
        $this->message->email = strip_tags($post['email'] ?? '');
        $this->message->subject =  strip_tags($post['subject'] ?? '');
        $this->message->country = function_exists('geoip_country_code_by_name')
            ? geoip_country_code_by_name($fromIpAddress) : 'XX';
        $this->message->fromId = $fromIpAddress . '/' . $this->message->country;
        $token = $post['next'] ?? '';

        $this->message->name = strip_tags($post['name'] ?? '');

        $this->message->message = strip_tags($post['message'] ?? '');
        $body = trim($this->message->message);
        if ($token !== $this->form->get('token')) {
            $this->message->alert = 'Sorry, it looks like your session expired.'
                . ' Please try again.';
            ($this->form->logger)('bad token: ' . $this->message->email);
            $this->message->status = 'fail';
            return $this->message;
        }

        $answer = (int) $post['demo'] ?? -1000;
        $base = $this->form->get('base');
        $correct = $this->form->get('correct');
        $fake = $this->form->get('fake');

        // Add a random delay to thwart execution time profiling.
        usleep(500_000 + mt_rand(0, 500_000));
        $wordCount = count(preg_split('/\s+/m', $body, -1, PREG_SPLIT_NO_EMPTY));
        if ($answer === $base + $fake) {
            (($this->form->logger))('bot');
            $this->message->status = 'fake';
        } elseif ($answer !== $base + $correct) {
            $this->message->attempts++;
            $this->message->alert = 'Sorry, your answer was incorrect. Please try again.';
            ($this->form->logger)('wrong ans');
            $this->message->status = 'fail';
        } elseif ($body === '') {
            $this->message->attempts++;
            $this->message->message = '';
            $this->message->alert = 'Sorry, the message is required. Please try again.';
            ($this->form->logger)('empty message');
            $this->message->status = 'fail';
        } elseif ($wordCount <= 2) {
            // Accept and discard all one and two word messages.
            ($this->form->logger)('short (faked)');
            $this->message->status = 'fake';
        } elseif ($this->isSpam()) {
            $this->message->status = 'spam';
        } else {
            ($this->form->logger)('Passed');
        }

        return $this->message;
    }

}
