<?php
declare(strict_types=1);

namespace Abivia\ContactSpam;

use Closure;
use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception as MailException;

class Form
{
    public string $baseUrl;
    private array $classes;
    private int $correct;
    private string $cssPath;

    public Closure $logger;

    public function cssPath(string $path = '')
    {
        $this->cssPath = $path;
    }

    public function emitBase()
    {
        echo $this->get('base');
    }

    public function emitClass(int $slot = null)
    {
        $slot ??= $this->correct;
        echo $this->classes[$slot];
    }

    public function emitFake()
    {
        echo " + " . $this->get('fake');
    }

    public function get($key, $default = null)
    {
        return $_SESSION[self::class][$key] ?? $default;
    }

    public function getCss()
    {
        $english = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten'];
        $css = [];
        $css[] = ".{$this->classes[0]}{left:-20000px;position:absolute}";
        for ($ind = 1; $ind < 11; ++$ind) {
            $css[] = ".{$this->classes[$ind]}:after{content:\" plus $english[$ind]\"}";
        }
        shuffle($css);
        $code = implode('', $css);
        if (($this->cssPath ?? '') === '') {
            echo "<style>$code</style>\n";
        } else {
            file_put_contents($this->baseUrl . $this->cssPath, $code);
        }

        return;
    }

    public function getLogger(): ?Closure
    {
        return $this->logger ?? null;
    }

    public function hasAlert()
    {
        $msg = $this->get('alert');
        return $msg !== null && $msg != '';
    }

    public function input(string $name)
    {
        echo $this->get($name, '');
    }

    public function logger(Closure $logger)
    {
        $this->logger = $logger;
    }

    private function pickClasses(string $prefix)
    {
        // Assign class names that generate a fake then the words "one" through "ten"
        $this->classes = [];
        for ($ind = 0; $ind < 11; ++$ind) {
            $name = '';
            for ($pos = 0; $pos < 5; ++$pos) {
                $name .= chr(ord('a') + random_int(0, 25));
            }
            $this->classes[] = $prefix . $name;
        }
        $this->put('classes', $this->classes);
    }

    public function preForm()
    {
        $this->put('token', bin2hex(random_bytes(24)));
        $this->put('base', random_int(10, 99));
        $this->correct = random_int(1, 9);
        $this->put('correct', $this->correct);
        $this->put('return', $_SERVER['SCRIPT_NAME']);
    }

    public function pull(string $key, $default = null)
    {
        $result = $_SESSION[self::class][$key] ?? $default;
        unset($_SESSION[self::class][$key]);
        return $result;
    }

    public function put($key, $value)
    {
        $_SESSION[self::class][$key] = $value;
    }

    public function redirect(string $target)
    {
        header("Location: $target");
        exit;
    }

    public function route(string $configPath, string $retryRelPath): ?Message
    {
        $score = new Score($this, $configPath);
        $message = $score->score($_POST);

        $_POST['next'] = '';
        $this->put('name', $message->name);
        $this->put('email', $message->email);
        $this->put('subject', $message->subject);
        $this->put('message', $message->message);
        $this->put('alert', $message->alert);
        $this->put('attempts', $message->attempts);

        if ($message->status === 'fail') {
            $this->redirect("{$this->baseUrl}$retryRelPath");
        } elseif ($message->status === 'good') {
            try {
                $this->send($message);
                // Message sent, clear our data from the session
                $_SESSION[static::class] = [];
            } catch (MailException $e) {
                $_SESSION[static::class] = [];
                $this->put('alert', 'Could not deliver your message.<br/>'
                    . '<br/>Please try again or reach us by phone.');
                return null;
            }
        }

        return $message;
    }

    private function send(Message $message)
    {
        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);
        // Server settings
        //$mail->SMTPDebug = 2;

        // Set mailer to use SMTP
        $mail->isSMTP();
        $mail->Host = 'mail.abivia.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'send.me.an.error@abivia.com';
        $mail->Password = 'sU2f3lxqJXVRnp5VASp77d_-69WcTmgT';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;

        // Recipients
        $mail->setFrom('site.com.rxp19@abivia.com', $message->name);
        $mail->addAddress('abivia.support@abivia.com');
        $mail->addReplyTo($message->email, $message->name);

        // Content
        $notes = [];
        if ($message->attempts > 1) {
            $notes[] = "a={$message->attempts}";
        }
        if ($message->score > 0.0) {
            $notes[] = "s={$message->score}";
        }
        $noteMsg = implode(', ', $notes);
        $mail->Subject = $message->fromId . ($noteMsg === '' ? '' : " ({$noteMsg})")
            . ' via Abivia.com Site: '
            . $message->subject;
        $mail->Body = $message->message;

        $mail->send();
    }

    /**
     * Get the singleton. If it doesn't exist, create and initialize it.
     * @param $baseUrl
     * @return static
     * @throws Exception
     */
    public static function singleton(?string $baseUrl = null, string $prefix = 'cca'): self
    {
        static $instance;

        if (isset($instance)) {
            return $instance;
        }
        $instance = new static();
        $instance->start();
        $instance->put('prefix', $prefix);
        $instance->pickClasses($prefix);
        if ($baseUrl === null) {
            $baseUrl = dirname($_SERVER['SCRIPT_NAME']);
        }
        if (str_ends_with($baseUrl, '/')) {
            $baseUrl = substr($baseUrl, 0, -1);
        }
        $instance->baseUrl = $baseUrl;

        return $instance;
    }

    /**
     * Make sure the session is started and the repository exists.
     *
     * @return void
     * @throws Exception
     */
    private function start()
    {
        switch (session_status()) {
            case PHP_SESSION_DISABLED:
                throw new Exception('Sessions are disabled');

            case PHP_SESSION_NONE:
                session_start();
                break;
        }
        if (isset($_SESSION[self::class])) {
            return;
        }
        $_SESSION[self::class] = [];
        self::put('attempts', 0);
        $this->put('fake', random_int(10, 29));
    }

}
